@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Täze Talyp</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                                           @csrf
                                             
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Login</label>
                                                <input  name="login" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Password</label>
                                                <input  name="password" type="password" class="form-control" required >
                                            </div>

                                             <div class="form-group">
                                                <label  class="control-label mb-1">Ady</label>
                                                <input  name="name" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Familiýasy</label>
                                                <input  name="last_name" type="text" class="form-control" required >
                                            </div>
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Telefon belgisi</label>
                                                <input  name="phone_number" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">E-mail</label>
                                                <input  name="email" type="text" class="form-control" required >
                                            </div>
                                            <div class=" form-group" id="category_2">

                                                    <label for="group" class=" form-control-label">Topary</label>
                                                        <select name="group" id="category_2" class="form-control-sm form-control"  >
                                                            @foreach($groups as $item)
                                                            <option value="{{$item->id}}">{{$item->group}}</option>
                                                            @endforeach
                                                        </select>

                                            </div>
                                            <div class="form-group"  style="padding-left: 1em">
                                                    <label for="profile-img-tag" class=" form-control-label">
                                                        Surat
                                                    </label>
                                                    <img src="" id="profile-img-tag" width="200px" />
                                                    <input type="file" name="image" id="profile-img" class="form-control-file"
                                                           accept="image/*" >
                                                </div>
                                           
                                            <div class="text-center">
                                                <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                                  Talyp goşmak
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>
<!-- 
                  <script>
                  function update() {

				var select = document.getElementById('category');
				var option = select.options[select.selectedIndex];

                if( option.value == "mollum"){
                    document.getElementById("category_2").style.display = "none";

                }else{
                    document.getElementById("category_2").style.display = "block";
                }
				
			}

			update();
                  </script> -->
               
@endsection






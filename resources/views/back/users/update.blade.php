@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Talybyň maglumatlaryny redaktirlemek</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('users.update',$user->id)}}" method="POST" enctype="multipart/form-data">
                                         @method('PUT') 
                                             @csrf
                                             
                                             <div class="form-group">
                                                <label  class="control-label mb-1">Login</label>
                                                <input  name="login" value="{{$user->login}}" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Password</label>
                                                <input  name="password" value="{{$user->password}}"  type="password" class="form-control" required >
                                            </div>

                                             <div class="form-group">
                                                <label  class="control-label mb-1"> Ady </label>
                                                <input  name="name" type="text" value="{{$user->name}}"  class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Familiýasy</label>
                                                <input  name="last_name" value="{{$user->last_name}}"  type="text" class="form-control" required >
                                            </div>
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Telefon belgisi</label>
                                                <input  name="phone_number" value="{{$user->phone_number}}" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">E-mail</label>
                                                <input  name="email" value="{{$user->email}}" type="text" class="form-control" required >
                                            </div>
                                           
                                            <div class=" form-group">

                                                    <label for="group"  class=" form-control-label">Topary</label>
                                                        <select name="group" id="category_2" class="form-control-sm form-control">
                                                            @foreach($groups as $item)
                                                            <option @if($user->group_id == $item->id) selected @endif value="{{$item->id}}">{{$item->group_number}}</option>
                                                            @endforeach
                                                        </select>

                                            </div>

                                            <div class="form-group" style="padding-left: 1em">
                                                    <label for="profile-img-tag" class=" form-control-label">
                                                        Surat
                                                    </label>
                                                    <img src="" id="profile-img-tag" width="200px" />
                                                    <input type="file" name="image" id="profile-img" class="form-control-file"
                                                           accept="image/*" >
                                                </div>

                                            <div class="text-center">
                                            <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                                  Täzele
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>

                  <!-- <script>

				var select = document.getElementById('category');
				var option = select.options[select.selectedIndex];
                
                if( option.value == "mollum"){
                   console.log("dasda");
                    document.getElementById("category_2").style.display = "none";

                }else{
                    document.getElementById("category_2").style.display = "block";
                }
				
		
                  </script> -->
@endsection


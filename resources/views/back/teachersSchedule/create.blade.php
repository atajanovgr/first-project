@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Mugallym üçin täze sapak</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('teacher_time_table.store')}}" method="POST" enctype="multipart/form-data">
                                           @csrf
                                             
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Gün</label>
                                                <input  name="day" type="number" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Tertip</label>
                                                <select  name="role" id="category" class="form-control-sm form-control">
                                                            <option  value="sanawjy" >Sanawjy</option>
                                                            <option  value="maydalawjy" >Maýdalawjy</option>
                                                        </select>
                                            </div>

                                             <div class="form-group">
                                                <label  class="control-label mb-1"> Irdenki Wagt</label>
                                                <input  name="time" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Öýlänki Wagt</label>
                                                <input  name="afternoon_time" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1"> Auditorýalar</label>
                                                <input  name="auditor" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Sapaklar</label>
                                                <input  name="lessons" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Sapaklaryň Görnüşleri</label>
                                                <input  name="type_lessons" type="text" class="form-control" required style="text-transform:uppercase" >
                                            </div>
                                        
                                            <div class=" form-group">

                                                <label for="group" class=" form-control-label">Toparlar</label>
                                                <input  name="groups" type="text" class="form-control" required >

                                                </div>

                                            <div class=" form-group">

                                                    <label for="group" class=" form-control-label">Mugallym</label>
                                                        <select name="teacher" id="category" class="form-control-sm form-control">
                                                            @foreach($teachers as $item)
                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                            @endforeach
                                                        </select>

                                            </div>                                           
                                             
                                            <div class="text-center">
                                                <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                                 Goşmak
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>
               
@endsection






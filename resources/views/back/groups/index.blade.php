@extends('back.layouts.master')
@section('content')
 <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{route('groups.create')}}"><div class="btn btn-sm btn-success">+ Täze Topar</div></a>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            
                                            <th>Toparyň nomeri</th>
                                            <th>Toparyň ady</th>
                                            <th>Fakultetiň ady</th>
                                            <th>Ýyly</th>
                                            <th>Goşmaçalar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($groups as $item)
                                        <tr>
                                             <td> {{ $item->group_number }} </td>
                                             <td> {{ $item->group_name }}   </td>
                                             <td> {{ $item->faculty_name }} </td>
                                             <td> {{ $item->year_number }}  </td>
                                            
                                            
                                             
                                            <td>
                                                <span class="product">
                                                <a href="{{route('groups.edit',$item->id)}}" title="Update" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square"></i></a>
                                                 <a href="{{route('groups.delete',$item->id)}}"  title="delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                   
                
               

                
                 
                


           

            

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
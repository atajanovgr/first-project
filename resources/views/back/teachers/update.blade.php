@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Mugallymyň maglumatlaryny redaktirlemek</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('teachers.update',$teacher->id)}}" method="POST" enctype="multipart/form-data">
                                         @method('PUT') 
                                             @csrf
                                             
                                             <div class="form-group">
                                                <label  class="control-label mb-1">Login</label>
                                                <input  name="login" value="{{$teacher->login}}" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Password</label> 
                                                <input  name="password" value="{{$teacher->password}}"  type="password" class="form-control" required >
                                            </div>

                                             <div class="form-group">
                                                <label  class="control-label mb-1">Ady</label>
                                                  <input  name="name" type="text" value="{{$teacher->name}}"  class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Familiýasy</label>
                                                <input  name="last_name" value="{{$teacher->last_name}}"  type="text" class="form-control" required >
                                            </div>
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Telefon belgisi</label>
                                                <input  name="phone_number" value="{{$teacher->phone_number}}" type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">E-mail</label>
                                                <input  name="email" value="{{$teacher->email}}" type="text" class="form-control" required >
                                            </div>
                                            <div class="form-group">

                                            <label for="group" class=" form-control-label">Kafedra</label>
                                            <input  name="kafedra" value="{{$teacher->kafedra}}" type="text" class="form-control" required >

                                            </div>
                                            <div class=" form-group" >

                                            <label for="group" class=" form-control-label">Wezipesi</label>
                                            <input  name="wezipe" value="{{$teacher->wezipe}}" type="text" class="form-control" required >

                                            </div>

                                            <div class="form-group" style="padding-left: 1em">
                                                    <label for="profile-img-tag" class=" form-control-label">
                                                        Surat
                                                    </label>
                                                    <img src="" id="profile-img-tag" width="200px" />
                                                    <input type="file" name="image" id="profile-img" class="form-control-file"
                                                           accept="image/*" >
                                                </div>

                                            <div class="text-center">
                                            <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                            Täzele
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>
@endsection


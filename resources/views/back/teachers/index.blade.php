@extends('back.layouts.master')
@section('content')
 <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{route('teachers.create')}}"><div class="btn btn-sm btn-success">+ Täze Mugallym</div></a>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                         
                                            <th>Surat</th>
                                            <th>Ady</th>
                                            <th>Familiýasy</th>
                                            <th>Telefon belgisi</th>
                                            <th>E-mail</th>
                                            <th>Kafedra</th>
                                            <th>Wezipesi</th>
                                            <th>Goşmaçalar</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($teachers as $item)
                                        <tr>
                                        <td class="avatar">
                                                <div class="round-img">
                                                    <img class="" src="{{asset($item->image)}}" alt="">
                                                </div>
                                        </td>

                                             <td> {{ $item->name }} </td>
                                             <td> {{ $item->last_name }}   </td>
                                             <td> {{ $item->phone_number }} </td>
                                             <td> {{ $item->email }}  </td>
                                             <td> {{ $item->kafedra}}  </td>
                                             <td> {{ $item->wezipe}}  </td>
                                            
                                            
                                             
                                            <td>
                                                <span class="product">
                                                <a href="{{route('teachers.edit',$item->id)}}" title="Update" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square"></i></a>
                                                 <a href="{{route('teachers.delete',$item->id)}}"  title="delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                   
                
               

                
                 
                


           

            

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
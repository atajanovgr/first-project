@extends('back.layouts.master')
@section('content')
 <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{route('time_table.create')}}"><div class="btn btn-sm btn-success">+ Täze Sapak</div></a>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                         
                                            <th>Günler</th>
                                            <th>Tertip</th>
                                            <th>Wagt</th>
                                            <th>Sapaklar</th>
                                            <th>Mugallymlar</th>
                                            <th>Topar nomer</th>
                                            <th>Goşmaçalar</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($time_table as $item)
                                        <tr>
                                        
                                             <td> {{ $item->day }} </td>
                                             <td> {{ $item->role }}   </td>
                                             <td>  <?php $time = json_decode($item->time, true); ?>  {{ $time }} </td>
                                             <td>  <?php $lessons = json_decode($item->lessons, true); ?> {{$lessons}} </td>
                                             <td>  <?php $teachers = json_decode($item->teachers, true); ?>{{ $teachers }}  </td>
                                             <td> {{ $item->group }}  </td>
                                            
                                            
                                             
                                            <td>
                                                <span class="product">
                                                <a href="{{route('time_table.edit',$item->id)}}" title="Update" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square"></i></a>
                                                 <a href="/admin/time_tables/delete/{{$item->id}}"  title="delete" class="btn btn-sm btn-danger">
                                                   <i class="fa fa-times"></i>
                                                </a>
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                   
                
               

                
                 
                


           

            

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
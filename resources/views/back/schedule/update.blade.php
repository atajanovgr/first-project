@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Sapaklary redaktirläň</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('time_table.update',$time_table->id)}}" method="POST" enctype="multipart/form-data">
                                         @method('PUT') 
                                             @csrf
                                             
                                             <div class="form-group">
                                                <label  class="control-label mb-1">Gün</label>
                                                <input  name="day" value="{{$time_table->day}}" type="text" class="form-control" required >
                                            </div>
                                                                                     
                                            <div class="form-group">
                                                    <label for="category" class=" form-control-label">Tertip</label>

                                                        <select  name="role" id="category" class="form-control-sm form-control">
                                                            <option @if($time_table->role == 'sanawjy') selected @endif value="sanawjy"> Sanawjy </option>
                                                            <option  @if($time_table->role == 'maydalawjy') selected @endif value="maydalawjy" >Maýdalawjy</option>
                                                        </select>

                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Irdenki Wagt</label>
                                                <?php $time = json_decode($time_table->time, true); ?> 
                                                <input  name="time" type="text" value="{{$time}}" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Öýlänki Wagt</label>
                                                <?php $time_ll = json_decode($time_table->afternoon_time, true); ?> 
                                                <input  name="afternoon_time" type="text" value="{{$time_ll}}" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1"> Auditorýalar </label>
                                                <?php $auditor = json_decode($time_table->auditor, true); ?> 
                                                <input  name="auditor" type="text"  value="{{$auditor}}" class="form-control" required >
                                            </div>
                                            
                                            <div class="form-group">
                                                <label  class="control-label mb-1">Sapaklar</label>
                                                <?php $lessons = json_decode($time_table->lessons, true); ?> 
                                                <input  name="lessons" value="{{$lessons}}"  type="text" class="form-control" required >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Sapaklaryň Görnüşleri</label>
                                                <?php $type_lessons = json_decode($time_table->type_lessons, true); ?> 
                                                <input  name="type_lessons" type="text" value="{{$type_lessons}}" class="form-control" required style="text-transform:uppercase" >
                                            </div>

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Mugallymlar</label>
                                                <?php $teachers = json_decode($time_table->teachers, true); ?> 
                                                <input  name="teachers" value="{{$teachers}}" type="text" class="form-control" required >
                                            </div>

                                            <div class=" form-group">

                                                    <label for="group" class=" form-control-label">Topary</label>
                                                        <select name="group" id="category" class="form-control-sm form-control">
                                                            @foreach($groups as $item)
                                                            <option @if($time_table->group_id == $item->id) selected @endif value="{{$item->id}}">{{$item->group_number}}</option>
                                                            @endforeach
                                                        </select>

                                            </div>

                                            <div class="text-center">
                                            <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                            Täzele
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>
@endsection


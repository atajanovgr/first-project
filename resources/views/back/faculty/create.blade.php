@extends('back.layouts.master')
@section('content')


                        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <!-- Credit Card -->
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{$error}}
                                @endforeach

                            </div>
                            @endif
                                        <div class="card-title">
                                            <h3 class="text-center">Täze Fakultet</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('faculty.store')}}" method="POST" enctype="multipart/form-data">
                                           @csrf
                                             

                                            <div class="form-group">
                                                <label  class="control-label mb-1">Fakultetiň ady</label>
                                                <input  name="name" type="text" class="form-control" required >
                                            </div>
                                         
                                           
                                            <div class="text-center">
                                                <button id="payment-button" type="submit" class="btn  btn-success btn-lg">
                                                 Fakultet goşmak
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- .card -->

                    </div><!--/.col-->
                  </div>
@endsection


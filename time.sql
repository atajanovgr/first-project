--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Ubuntu 14.1-2.pgdg20.04+1)
-- Dumped by pg_dump version 14.1 (Ubuntu 14.1-2.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: faculties; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.faculties (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.faculties OWNER TO admin;

--
-- Name: faculties_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.faculties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.faculties_id_seq OWNER TO admin;

--
-- Name: faculties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.faculties_id_seq OWNED BY public.faculties.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.groups (
    id bigint NOT NULL,
    group_number character varying(255) NOT NULL,
    group_name character varying(255) NOT NULL,
    faculty_name character varying(255) NOT NULL,
    year_number character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    faculty_id bigint
);


ALTER TABLE public.groups OWNER TO admin;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO admin;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO admin;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO admin;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO admin;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO admin;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- Name: teacher_schedules; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.teacher_schedules (
    id bigint NOT NULL,
    day character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    "time" json NOT NULL,
    afternoon_time json NOT NULL,
    lessons json NOT NULL,
    type_lessons json NOT NULL,
    auditor json NOT NULL,
    teacher_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "group" json NOT NULL
);


ALTER TABLE public.teacher_schedules OWNER TO admin;

--
-- Name: teacher_schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.teacher_schedules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teacher_schedules_id_seq OWNER TO admin;

--
-- Name: teacher_schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.teacher_schedules_id_seq OWNED BY public.teacher_schedules.id;


--
-- Name: teachers; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.teachers (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    image character varying(255) NOT NULL,
    kafedra character varying(255) NOT NULL,
    wezipe character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role character varying(255) DEFAULT 'mollum'::character varying NOT NULL
);


ALTER TABLE public.teachers OWNER TO admin;

--
-- Name: teachers_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.teachers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teachers_id_seq OWNER TO admin;

--
-- Name: teachers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.teachers_id_seq OWNED BY public.teachers.id;


--
-- Name: time_tables; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.time_tables (
    id bigint NOT NULL,
    day character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    "time" json NOT NULL,
    lessons json NOT NULL,
    teachers json NOT NULL,
    group_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    afternoon_time json NOT NULL,
    auditor json NOT NULL,
    type_lessons json NOT NULL
);


ALTER TABLE public.time_tables OWNER TO admin;

--
-- Name: time_tables_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.time_tables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.time_tables_id_seq OWNER TO admin;

--
-- Name: time_tables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.time_tables_id_seq OWNED BY public.time_tables.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    image character varying(255) NOT NULL,
    group_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role character varying(255) DEFAULT 'talyp'::character varying NOT NULL
);


ALTER TABLE public.users OWNER TO admin;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO admin;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: faculties id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.faculties ALTER COLUMN id SET DEFAULT nextval('public.faculties_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- Name: teacher_schedules id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.teacher_schedules ALTER COLUMN id SET DEFAULT nextval('public.teacher_schedules_id_seq'::regclass);


--
-- Name: teachers id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.teachers ALTER COLUMN id SET DEFAULT nextval('public.teachers_id_seq'::regclass);


--
-- Name: time_tables id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.time_tables ALTER COLUMN id SET DEFAULT nextval('public.time_tables_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: faculties; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.faculties (id, name, created_at, updated_at) FROM stdin;
4	Menejment	2021-12-24 14:02:46	2021-12-24 14:02:46
5	Maliye	2021-12-24 14:02:54	2021-12-24 14:02:54
10	Ykdysadyýet	2022-01-14 08:55:06	2022-01-14 08:55:06
11	Marketing	2022-01-15 09:44:42	2022-01-15 09:44:42
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.groups (id, group_number, group_name, faculty_name, year_number, created_at, updated_at, faculty_id) FROM stdin;
17	2421-B	vfdagfdsg	Maliye	3	2021-12-24 14:14:27	2021-12-25 12:18:19	\N
19	2411-B	AU we KTPU	Menejment	2	2021-12-25 13:34:46	2021-12-25 13:34:46	\N
22	0111-B	1	Ykdysadyýet	1	2022-01-14 08:55:43	2022-01-14 08:55:43	\N
23	0211-B	2	Ykdysadyýet	2	2022-01-14 08:56:06	2022-01-14 08:56:06	\N
24	0411-B	4	Ykdysadyýet	4	2022-01-14 08:56:21	2022-01-14 08:56:21	\N
25	2258	HOBAS	Marketing	5	2022-01-15 09:45:35	2022-01-15 09:45:35	\N
26	2235	RUT	Marketing	3	2022-01-15 09:58:22	2022-01-15 09:58:22	\N
18	1112-B	AU we KTPU	Menejment	2	2021-12-24 14:29:37	2022-01-18 14:12:32	\N
20	2431-B	Hünärmen	Menejment	1	2021-12-25 13:34:56	2022-01-18 14:13:19	\N
27	0312-H	Kompyuter ulgamynyn programma upjunciligi	Ykdysadyýet	3	2022-01-18 14:17:52	2022-01-18 14:17:52	\N
29	2413	H	Menejment	3	2022-01-18 15:06:27	2022-01-18 15:06:27	\N
30	12345	H	Menejment	4	2022-01-18 15:06:44	2022-01-18 15:06:44	\N
31	2413	H	Menejment	5	2022-01-18 15:06:55	2022-01-18 15:06:55	\N
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2019_12_14_000001_create_personal_access_tokens_table	1
2	2021_12_08_120923_create_groups_table	2
3	2021_12_08_130120_create_users_table	3
4	2021_12_08_150359_create_time_tables_table	4
5	2021_12_21_102829_add_timeauditor_to_time_tables_table	5
6	2021_12_21_120822_add_amalyiliumumy_to_time_tables_table	6
7	2021_12_24_131024_create_faculties_table	7
8	2021_12_24_131202_add_paid_to_groups_table	7
9	2022_01_07_134215_create_teachers_table	8
10	2022_01_07_154709_create_teacher_schedules_table	9
11	2022_01_07_162430_add_group_to_teacher_schedules_table	10
12	2022_01_08_103734_add_role_to_teachers_table	11
13	2022_01_08_104331_add_role_to_users_table	12
\.


--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: teacher_schedules; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.teacher_schedules (id, day, role, "time", afternoon_time, lessons, type_lessons, auditor, teacher_id, created_at, updated_at, "group") FROM stdin;
1	3	sanawjy	"12:00,13:00,14:00,15:00,16:00,17:00"	"12:00,13:00,14:00,15:00,16:00,17:00"	"Maliye,makroyd,mikroyk,hytay dili"	"a,u,t"	"3345,3455,34534"	1	2022-01-07 16:24:56	2022-01-07 16:24:56	"2411-H,2431-H,2421-H"
2	1	sanawjy	"12:00,13:00,14:00,15:00,16:00,17:00"	"12:00,13:00,14:00,15:00,16:00,17:00"	"Maliye,makroyd,mikroyk,hytay dili"	"a,u,t"	"3345,3455,34534"	1	2022-01-07 16:25:28	2022-01-07 16:25:28	"2411-H,2431-H,2421-H"
3	4	sanawjy	"08:10-08:50,09:00-09:40"	"10:50-11:30,11:40-12:20"	"D\\u00fcn\\u00fd\\u00e4 ykdysady\\u00fdeti,D\\u00fcn\\u00fd\\u00e4 ykdysady\\u00fdeti"	"a,a"	"4219,44003"	1	2022-01-13 15:25:48	2022-01-13 15:25:48	"2411B,2415"
10	3	maydalawjy	"12:00,13:00,14:00,15:00,16:00,17:00"	"12:00,13:00,14:00,15:00,16:00,17:00"	"Maliye,makroyd,mikroyk,hytay dili"	"a,u,t"	"3345,3455,34534"	2	2022-01-14 10:24:43	2022-01-14 10:24:43	"2411-H,2431-H,2421-H"
11	5	maydalawjy	"08:10-08:50,09:00-09:40"	"10:50-11:30,11:40-12:20"	"ykdysady nazaryyet, yokary matematika"	"a,u"	"4103,4103"	1	2022-01-14 10:27:37	2022-01-14 10:27:37	"2411-B,2415"
13	2	sanawjy	"08:10-08:50,09:00-09:40,09:50-10:30"	"16:00-16:40,16:50-17:30,17:40-18:20"	"D\\u00fcn\\u00fd\\u00e4 ykdysady\\u00fdeti,Sy\\u00fdasaty \\u00f6wreni\\u015f,D\\u00fcn\\u00fd\\u00e4 ykdysady\\u00fdeti"	"amaly,umumy,amaly"	"4103,2511,4211"	3	2022-01-18 14:51:36	2022-01-18 14:51:36	"2411-B,0211-H,2415"
14	4	maydalawjy	"08:10-08:50,09:00-09:40"	"16:00-16:40,16:50-17:30"	"Ykdysady\\u00fdeti\\u0148 nazary\\u00fdeti,D\\u00fcn\\u00fd\\u00e4 ykdysady\\u00fdeti"	"amaly,amaly"	"4103,4218"	3	2022-01-18 14:53:15	2022-01-18 14:53:15	"0411-H,5211-B"
\.


--
-- Data for Name: teachers; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.teachers (id, name, last_name, login, password, phone_number, email, image, kafedra, wezipe, created_at, updated_at, role) FROM stdin;
1	qwerty	qwerty	qwerty	$2y$10$UxZOAq0GzHsz8en.W0yN2OAj9xWdJYIsDl.7fBORZjwSCV5PQnfOi	+96663123456	test@example.com	/uploads/1641570297image.png	blmk	error	2022-01-07 15:44:57	2022-01-07 15:44:57	mollum
2	Baidu	myradov	t	$2y$10$cLudslAkokINwrFFWjChWu73iM38SMtPkYPfm1hQLQMOWmzMu1dVS	+993365478995	world-2022@mail.ru	/uploads/1641893820image.jpg	kaffa	wezzzw	2022-01-11 09:37:00	2022-01-11 09:37:00	mollum
3	Myrat	Nurýagdyýew	Salam_mugallym	$2y$10$Yav0z6Q8ZakZrEHApQFCCeLg9IQJjXNNVRtJSp7YQOOGCmkG8ItK.	+99363xxxxxx	myrat_salam_tm@gmail.сom	/uploads/1642516320image.png	Maglumat ulgamlary	Uly mugallym	2022-01-18 14:32:00	2022-01-18 14:32:00	mollum
4	Berdi	Berdiyew	QWERTY	$2y$10$2/.Ec6xzsqIbJB0FQA0y5.Ffh4t.ozfMT0eMkpet9SXLpXKmFqVp6	+966631234562	test@example.com	/uploads/1642933404image.png	blmk	error	2022-01-23 10:23:24	2022-01-23 10:23:24	mollum
\.


--
-- Data for Name: time_tables; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.time_tables (id, day, role, "time", lessons, teachers, group_id, created_at, updated_at, afternoon_time, auditor, type_lessons) FROM stdin;
25	5	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"D\\\\u00fcn\\\\u00fd\\\\u00e4 ykdysady\\\\u00fdeti,Sy\\\\u00fdasaty \\\\u00f6wreni\\\\u015f\\""	"\\"Berdi\\\\u00fdew M.G,Gurbanowa J.B.\\""	19	2022-01-18 14:46:28	2022-01-18 14:46:28	"16:00-16:40,16:50-17:30"	"411,109"	"amaly,amaly"
26	2	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"Bedenterbi\\\\u00fde,Sanly ykdysady\\\\u00fdet\\""	"\\"Gazakow E.,uly mugallym Durdy\\\\u00fdewa O.K.\\""	24	2022-01-18 15:11:18	2022-01-18 15:11:18	"16:00-16:40,16:50-17:30"	"sport zal,4103"	"amaly,amaly"
27	3	sanawjy	"\\"08:10-08:50,09:00-09:40,09:50-10:30\\""	"\\"Fizika,\\\\u00ddokary matematika,Psihologi\\\\u00fda\\""	"\\"t.y.k. uly mugallym Allabergenow A.M,uly mugallym \\\\u00d6wezowa G,Annany\\\\u00fdazow D.\\""	24	2022-01-18 15:12:47	2022-01-18 15:12:47	"16:00-16:40,16:50-17:30,17:40-18:20"	"4103,4218,2458"	"amaly,umumy,amaly"
14	3	sanawjy	"\\"12:00,13:00,14:00,15:00,16:00,17:00\\""	"\\"Maliye,makroyd,mikroyk,hytay dili\\""	"\\"myrat,merdan,jemal,mergen\\""	18	2021-12-24 20:49:35	2021-12-24 20:49:35	"12:00,13:00,14:00,15:00,16:00,17:00"	"3345,3455,34534"	"a,u,t"
15	1	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"ykdysady nazaryyet, yokary matematika,\\""	"\\"Hanguly\\\\u00fdewa O.B, A\\\\u00fdnazarow A.A\\""	19	2022-01-08 11:08:53	2022-01-08 11:08:53	"10:50-11:30,11:40-12:20"	"4219,44003"	"a,u"
17	3	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"T\\\\u00fcrkmenistany\\\\u0148 Taryhy,H\\\\u00e4zirki zaman komp\\\\u00fduter tehnologi\\\\u00fdalary\\""	"\\"Amangeldi\\\\u00fdewa R.G., uly mugallym Annaorazowa M.\\""	19	2022-01-09 09:35:00	2022-01-09 09:35:00	"10:50-11:30,11:40-12:20"	"4219,44003"	"a,u"
18	1	maydalawjy	"\\"12:00,13:00,14:00,15:00,16:00,17:00\\""	"\\"Maliye,makroyd,mikroyk,hytay dili\\""	"\\"myrat,merdan,jemal,mergen\\""	19	2022-01-09 11:50:19	2022-01-09 11:50:19	"12:00,13:00,14:00,15:00,16:00,17:00"	"3345,3455,34534"	"a,u,t"
22	5	maydalawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"ykdysady nazaryyet, yokary matematika,\\""	"\\"trtgr,asdsa\\""	24	2022-01-14 08:57:38	2022-01-14 08:57:38	"10:50-11:30,11:40-12:20"	"411,229"	"a,u"
23	2	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"Ta\\\\u00fd\\\\u00fdarlygy\\\\u0148 ugry bo\\\\u00fdun\\\\u00e7a I\\\\u0148lis dili,Ykdysady nazary\\\\u00fdet we ykdysady taglymatlary\\\\u0148 nazary\\\\u00fdeti\\""	"\\"Hanguly\\\\u00fdewa O.B,uly mugallym A\\\\u00fdnazarow A.A\\""	19	2022-01-18 14:39:05	2022-01-18 14:39:05	"16:00-16:40,16:50-17:30"	"4103,2511"	"amaly,umumy"
24	2	maydalawjy	"\\"08:10-08:50,09:00-09:40,09:50-10:30\\""	"\\"T\\\\u00fcrkmenistany\\\\u0148 Taryhy,Bedenterbi\\\\u00fde,H\\\\u00e4zirki zaman komp\\\\u00fduter tehnologi\\\\u00fdalary\\""	"\\"Amangeldi\\\\u00fdewa R.G.,Gazakow E.,uly mugallym Annaorazowa M.\\""	19	2022-01-18 14:41:00	2022-01-18 14:41:00	"16:00-16:40,16:50-17:30,17:40-18:20"	"4219,sport zal,3422"	"amaly,amaly,tejribe"
30	4	sanawjy	"\\"08:10-08:50,09:00-09:40\\""	"\\"ykdysady nazaryyet, yokary matematika,\\""	"\\"Hanguly\\\\u00fdewa O.B, A\\\\u00fdnazarow A.A\\""	19	2022-01-20 11:25:17	2022-01-20 11:25:17	"10:50-11:30,11:40-12:20"	"4219,44003"	"a,a"
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.users (id, name, last_name, login, password, phone_number, email, image, group_id, created_at, updated_at, role) FROM stdin;
34	Kuwwat	Donmezow	g	$2y$10$64xFrt/jKJF8ri70v2ssNuQ1mCKIvwrtoq7WsnrVuwpEGcfSzXt0G	+99364848483	donmez@gmail.com	/uploads/1642241488image.png	23	2022-01-15 10:11:28	2022-01-15 10:11:28	talyp
35	Arslan	Gaplan	h	$2y$10$Y5l7AHLXb.U8O8n7zxoO7uquZ0KF7dr9T1ArVfbNsxbhddMae.ZLW	65889874	menwesen@gmail.com	/uploads/1642241573image.png	22	2022-01-15 10:12:53	2022-01-15 10:12:53	talyp
27	qwerty	qwerty	qwerty	$2y$10$fsPp5yQWlHszkHunWH6Ue.LROyItGhzC2uSI2K7pXBzO1fFmfMhkm	+96663123456	atajanovgr@gmail.com	/uploads/1641562498image.png	20	2022-01-07 13:21:45	2022-01-07 13:35:34	talyp
29	Akmyrat	Akmyradow	a	$2y$10$mur.GgqN7XpNn2PqhMUk6OWLLjlbfiCc0aisDlrwb1jx7tst7BRJu	65889874	akmyrat@gmail.com	/uploads/1642241250image.png	26	2022-01-15 10:07:30	2022-01-15 10:07:30	talyp
30	Men	Sen	s	$2y$10$WQi/2e94tmdVG3EFxyApjOQOA1TbY1zVu.uZgmSPwH.DKQWOiXoXu	77896541	menwesen@gmail.com	/uploads/1642241303image.png	25	2022-01-15 10:08:23	2022-01-15 10:08:23	talyp
31	Dawut	Rahmanow	d	$2y$10$I2iLpPgIzLvhVzr9TO0Tce3Ty96pdCuwNLo6CMVmIiNo/P14eRkqi	+99364848483	dawut@mail.com	/uploads/1642241349image.png	24	2022-01-15 10:09:09	2022-01-15 10:09:09	talyp
32	Azat	Donmezow	f	$2y$10$xXH39tU6ul20M4kOOyJrw.2ZR6XmSc5SNmpaeVrwZIbsEn2CvpfLi	+99365555555	donmez@gmail.com	/uploads/1642241429image.png	24	2022-01-15 10:10:29	2022-01-15 10:10:29	talyp
28	Mekan	Ataýew	Salam_talyp	$2y$10$QHPxaDwtotSmQqyxMdhvse9326Vh75/4NoaX1hwdFpO5kU56sv2tC	+99363xxxxxx	salam_HJ@gmail.com	/uploads/1642516033image.png	19	2022-01-08 10:36:37	2022-01-18 14:28:12	talyp
37	Kakajan	Nohurb	k	$2y$10$.0gSApKDZnyiljTsz.mJJOiCKL/S7j6HER6x3KZMqUp1GdmY5JtAK	+99364848483	dawut@mail.com	/uploads/1642241675image.png	17	2022-01-15 10:14:35	2022-01-18 14:56:35	talyp
36	Azat	Meredow	j	$2y$10$181WICcMR7qo5p8IJ4FuD.nzCtb0TfcllogAL1Ey2fvIfCiB9UmZe	+99363047340	a_meredow@salam.tm	/uploads/1642517866image.png	19	2022-01-15 10:13:44	2022-01-18 15:03:19	talyp
38	qwerty123	qwerty123	qwerty123	$2y$10$ZfBU8ux4aoSkw3UGEr88IO1LWUn4E1O5TaaxthSbSAxXQRg2BBh.W	+966631234562	test@example.com	/uploads/1642930036image.jpeg	31	2022-01-23 09:27:16	2022-01-23 09:27:16	talyp
\.


--
-- Name: faculties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.faculties_id_seq', 11, true);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.groups_id_seq', 31, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.migrations_id_seq', 13, true);


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);


--
-- Name: teacher_schedules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.teacher_schedules_id_seq', 14, true);


--
-- Name: teachers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.teachers_id_seq', 4, true);


--
-- Name: time_tables_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.time_tables_id_seq', 30, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.users_id_seq', 38, true);


--
-- Name: faculties faculties_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.faculties
    ADD CONSTRAINT faculties_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- Name: teacher_schedules teacher_schedules_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.teacher_schedules
    ADD CONSTRAINT teacher_schedules_pkey PRIMARY KEY (id);


--
-- Name: teachers teachers_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_pkey PRIMARY KEY (id);


--
-- Name: time_tables time_tables_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.time_tables
    ADD CONSTRAINT time_tables_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- Name: groups groups_faculty_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_faculty_id_foreign FOREIGN KEY (faculty_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: teacher_schedules teacher_schedules_teacher_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.teacher_schedules
    ADD CONSTRAINT teacher_schedules_teacher_id_foreign FOREIGN KEY (teacher_id) REFERENCES public.teachers(id) ON DELETE CASCADE;


--
-- Name: time_tables time_tables_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.time_tables
    ADD CONSTRAINT time_tables_group_id_foreign FOREIGN KEY (group_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: users users_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_group_id_foreign FOREIGN KEY (group_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


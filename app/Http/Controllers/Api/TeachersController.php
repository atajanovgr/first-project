<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Crypt;
use App\Models\Teacher;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::get();
        return view('back.teachers.index',compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'last_name' => 'required',
                'kafedra' => 'required',
                'wezipe' => 'required',
                'login' => 'required',
                'password' => 'required',
                'phone_number' => 'required',
                'email' => 'required|email',
                'image' => 'required|image|mimes:jpg,jpeg,png,svg|max:2048',
            
            ]
        );
         
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }



        $user = new Teacher;
        $user->name = $request->name;
        $user->last_name = $request->last_name;

        $user->login = $request->login;
        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phone_number;

        $user->email = $request->email;
        $user->wezipe = $request->wezipe;
        $user->kafedra = $request->kafedra;
       

    if($request->hasFile('image')){
        $imageName = time().'image.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads'),$imageName);
        $user->image='/uploads/'.$imageName;
    }

        $user->save();

        return redirect()->route('teachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $teacher = Teacher::findOrFail($id);
        return view('back.teachers.update',compact('teacher'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'last_name' => 'required',
                'kafedra' => 'required',
                'wezipe' => 'required',
                'login' => 'required',
                'password' => 'required',
                'phone_number' => 'required',
                'email' => 'required|email',
                'image' => 'image|mimes:jpg,jpeg,png,svg|max:2048',
            
            ]
        );
         
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }
        $teacher = Teacher::findOrFail($id);
        if($teacher == null){
            return redirect()->route('teachers.index');
        }else{

        $teacher->name = $request->name;
        $teacher->last_name = $request->last_name;

        $teacher->login = $request->login;
        $teacher->password = bcrypt($request->password);
        $teacher->phone_number = $request->phone_number;

        $teacher->email = $request->email;
        $teacher->wezipe = $request->wezipe;
        $teacher->kafedra = $request->kafedra;
       

    if($request->hasFile('image')){
        $imageName = time().'image.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads'),$imageName);
        $teacher->image='/uploads/'.$imageName;
    }

        $teacher->save();

        return redirect()->route('teachers.index');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $teacher = Teacher::where('id',$id)->first();

        $teacher->forceDelete();

        return redirect()->route('teachers.index');
    }
}

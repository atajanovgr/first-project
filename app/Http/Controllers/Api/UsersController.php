<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Group;
use Illuminate\Support\Facades\DB;
use Validator;
use Crypt;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->select('users.*', 'groups.group_number as group')
        ->orderBy('users.id', 'desc')
        ->get();

         return view('back.users.index',compact('users'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $groups = DB::table('groups')
        ->select('groups.group_number as group','groups.id')
        ->orderBy('groups.id', 'desc')
        ->get();

        return view('back.users.create',compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'last_name' => 'required',
                'login' => 'required',
                'password' => 'required',
                'phone_number' => 'required',
                'email' => 'required|email',
                'group' => 'required',
                'image' => 'required|image|mimes:jpg,jpeg,png,svg|max:2048',
            
            ]
        );
         
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }



        $user = new User;
        $user->name = $request->name;
        $user->last_name = $request->last_name;

        $user->login = $request->login;
        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phone_number;

        $user->email = $request->email;
        $user->group_id = $request->group;
       

    if($request->hasFile('image')){
        $imageName = time().'image.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads'),$imageName);
        $user->image='/uploads/'.$imageName;
    }

        $user->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $groups = Group::get();
        
        return view('back.users.update',compact('groups','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
               
        $validator = Validator::make(
            $request->all(),
            [

                'name' => 'required',
                'last_name' => 'required',
                'login' => 'required',
                'password' => 'required',
                'phone_number' => 'required',
                'email' => 'required|email',
                'group' => 'required',
                'image' => 'image|mimes:jpg,jpeg,png,svg|max:2048',

            ]
        );


        if($validator->fails()){
            return redirect()->route('users.index');
        }


        $user = User::findOrFail($id);

        if($user == null){
            return redirect()->route('users.index');
        }else{

        $user->name = $request->name;
        $user->last_name = $request->last_name;

        $user->login = $request->login;
        $user->password = bcrypt($request->password);
        
        
        $user->phone_number = $request->phone_number;

        $user->email = $request->email;
        $user->group_id = $request->group;
       

        if($request->hasFile('image')){
            $imageName = time().'image.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads'),$imageName);
            $user->image='/uploads/'.$imageName;
        }
            $user->save();


            return redirect()->route('users.index');
    }}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $group = User::where('id',$id)->first();

        $group->forceDelete();

        return redirect()->route('users.index');
    }
}

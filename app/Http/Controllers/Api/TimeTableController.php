<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TimeTable;
use App\Models\Group;
use Illuminate\Support\Facades\DB;
use Validator;

class TimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $time_table = DB::table('time_tables')
        ->leftJoin('groups', 'groups.id', '=', 'time_tables.group_id')
        ->select('time_tables.*', 'groups.group_number as group')
        ->orderBy('time_tables.id', 'desc')
        ->get();
       $test = json_decode($time_table, true);
       // dd($test);
        return  view('back.schedule.index',compact('time_table'));
    }
        

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = DB::table('groups')
        ->select('groups.group_number as group','groups.id')
        ->orderBy('groups.id', 'desc')
        ->get();

        return view('back.schedule.create',compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make(
            $request->all(),
            [
                'day' => 'required',
                'role' => 'required',
                'time' => 'required',
                'auditor' => 'required',
                'afternoon_time' => 'required',
                'lessons' => 'required',
                'type_lessons' => 'required',
                'teachers' => 'required',
                'group' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }
       
        $user = new TimeTable;
        $user->day = $request->day;
        $user->role = $request->role;

        $user->time = json_encode($request->time);
        $user->auditor = json_encode($request->auditor);
        $user->afternoon_time = json_encode($request->afternoon_time);
        $user->lessons = json_encode($request->lessons);
        $user->type_lessons = json_encode($request->type_lessons);
        $user->teachers = json_encode($request->teachers);
        
        $user->group_id = $request->group;

        $user->save();

        return redirect()->route('time_table.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $time_table = TimeTable::findOrFail($id);
        $groups = Group::get();
        
        return view('back.schedule.update',compact('groups','time_table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'day' => 'required',
                'role' => 'required',
                'time' => 'required',
                'lessons' => 'required',
                'teachers' => 'required',
                'auditor' => 'required',
                'afternoon_time' => 'required',
                'type_lessons' => 'required',
                'group' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }
        $user = TimeTable::findOrFail($id);

        if($user == null){
            return redirect()->route('time_table.index');
        }else{
        $user->day = $request->day;
        $user->role = $request->role;

        $user->time = json_encode($request->time);
        $user->lessons = json_encode($request->lessons);
        $user->teachers = json_encode($request->teachers);

        $user->auditor = json_encode($request->auditor);
        $user->afternoon_time = json_encode($request->afternoon_time);
        $user->type_lessons = json_encode($request->type_lessons);
        $user->group_id = $request->group;

        $user->save();
        
        return redirect()->route('time_table.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $group = TimeTable::where('id',$id)->firstOrFail();

            $group->forceDelete();

            return redirect()->route('time_table.index');
    }
}

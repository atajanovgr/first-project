<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Faculty;
use Validator;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        //dd($groups);

        return view('back.groups.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = Faculty::get();
        return view('back.groups.create',compact('name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'group_number' => 'required',
                'group_name' =>'required',
                'faculty_name' => 'required',
                'year_number' => 'required'
            ]
        );
        
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }

        $name = Faculty::where('id',$request->faculty_name)->select('name')->first();

        $group = new Group;
        $group->group_number = $request->group_number;
        $group->group_name = $request->group_name;
        $group->faculty_name = $name->name;
        $group->year_number = $request->year_number;
        $group->save();

        //dd($group);

        return redirect()->route('groups.index');

        // return [
        //     'status' => true,
        //     'data' => 'successfully'
        // ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);
        $name = Faculty::get();
        
        return view('back.groups.update',compact('group','name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //dd($request->all());
        $validator = Validator::make(
            $request->all(),
            [

                'group_number' => 'required',
                'group_name' => 'required',
                'faculty_name' => 'required',
                'year_number' => 'required',

            ]
        );



        if($validator->fails()){
            return redirect()->route('groups.index');
        }


        $group = Group::findOrFail($id);

        if($group == null){
            return redirect()->route('groups.index');
        }else{
            $name = Faculty::where('id',$request->faculty_name)->select('name')->first();

            $group->group_number = $request->group_number;
            $group->group_name = $request->group_name;
            $group->faculty_name = $name->name;
            $group->year_number = $request->year_number;
            $group->save();

            return redirect()->route('groups.index');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $group = Group::where('id',$id)->first();

            $group->forceDelete();

            return redirect()->route('groups.index');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TeacherSchedule;
use App\Models\Teacher;
use Illuminate\Support\Facades\DB;
use Validator;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $time_table = DB::table('teacher_schedules')
        ->leftJoin('teachers', 'teachers.id', '=', 'teacher_schedules.teacher_id')
        ->select('teacher_schedules.*', 'teachers.name as name')
        ->orderBy('teacher_schedules.id', 'desc')
        ->get();
       $test = json_decode($time_table, true);
       // dd($test);
        return  view('back.teachersSchedule.index',compact('time_table'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = DB::table('teachers')
        ->select('teachers.name as name','teachers.id')
        ->orderBy('teachers.id', 'desc')
        ->get();
        return view('back.teachersSchedule.create',compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        $validator = Validator::make(
            $request->all(),
            [ 

            'day' => 'required',
            'role' => 'required',
            'time' => 'required',
            'auditor' => 'required',
            'afternoon_time' => 'required',
            'lessons' => 'required',
            'type_lessons' => 'required',
            'groups' => 'required',
            'teacher' => 'required',

            ]
        );
         
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }



        $user = new TeacherSchedule;
        $user->day = $request->day;
        $user->role = $request->role;

        $user->time = json_encode($request->time);
        $user->auditor = json_encode($request->auditor);
        $user->afternoon_time = json_encode($request->afternoon_time);
        
        $user->lessons = json_encode($request->lessons);
        $user->type_lessons = json_encode($request->type_lessons);
        
        $user->group = json_encode($request->groups);
        $user->teacher_id = $request->teacher;
        $user->save();

        return redirect()->route('teacher_time_table.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $time_table = TeacherSchedule::findOrFail($id);
        $teachers = Teacher::get();
        return view('back.teachersSchedule.update',compact('time_table','teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [ 

            'day' => 'required',
            'role' => 'required',
            'time' => 'required',
            'auditor' => 'required',
            'afternoon_time' => 'required',
            'lessons' => 'required',
            'type_lessons' => 'required',
            'groups' => 'required',
            'teacher' => 'required',

            ]
        );
         
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->messages()
            ];
        }

        $teacher = TeacherSchedule::findOrFail($id);

        if($teacher == null){
            return redirect()->route('teachersSchedule.index');
        }else{
            $teacher->day = $request->day;
            $teacher->role = $request->role;
    
            $teacher->time = json_encode($request->time);
            $teacher->auditor = json_encode($request->auditor);
            $teacher->afternoon_time = json_encode($request->afternoon_time);
            
            $teacher->lessons = json_encode($request->lessons);
            $teacher->type_lessons = json_encode($request->type_lessons);
            
            $teacher->group = json_encode($request->groups);
            $teacher->teacher_id = $request->teacher;
            $teacher->save();
    
            return redirect()->route('teacher_time_table.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function delete($id)
    {
        $group = TeacherSchedule::where('id',$id)->firstOrFail();

            $group->forceDelete();

            return redirect()->route('teacher_time_table.index');
    }
}

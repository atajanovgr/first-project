<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Group;
use App\Models\Teacher;
use App\Models\TimeTable;
use App\Models\Faculty;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\TeacherSchedule;
use Illuminate\Support\Facades\RateLimiter;


class AuthController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:users', ['except' => ['login']]);
  }

  public function login(Request $request)
  {
    $validator = Validator::make(
      $request->all(),
      [
        'password' => 'required',
        'login' => 'required',
      ]
    );

    if ($validator->fails()) {
      return [
        'status' => false,
        'errors' => 'Ahli polyalary dolduryn!!!'
      ];
    }
    $credentials = request(['login', 'password']);
  
   $user = Auth::guard('users')->attempt($credentials);
   $teacher = Auth::guard('teachers')->attempt($credentials);

        if ($user == false && $teacher == false) {

          return response()->json([
            'status' =>false,
            'error' => 'Unauthorized'], 401);
        } 
         elseif ($user == true) {
           
              
          $data = User::with('getGroup')->where('login',$request->login)->first();

          return $this->respondWithUser($user,$data);
        } 
         elseif ($teacher == true) {
          $data = Teacher::where('login',$request->login)->first();
          return $this->respondWithTeacher($teacher,$data);
        }
  }

  public function getFaculty()
  {
    
    $faculty = Faculty::get();

    $dd = [];
    $count = 0;

    foreach ($faculty as  $item) {

      $dd[$count]['name'] = $item->name;

      $count++;
    }



    return [
      'status' => true,
      'data' => $dd
    ];
  }

  public function  getGroups(Request $request)
  {
    //dd($request->name);
    $groups = Group::where('faculty_name', $request->name)->select('group_number', 'year_number')->get();
    $groups->toArray();

    $object = collect($groups);
    $grouped = $object->groupBy('year_number');
    $grouped->all();

    return [
      'status' => true,
      'data' => $groups
    ];
  }

  public function getData(Request $request)
  {

    if ($request->group_number !== null) {
      $group = Group::with('getTimeTable')->where('group_number', $request->group_number)->firstOrFail();
      $get_group = Group::where('group_number', $request->group_number)->first();
      $time = TimeTable::where('group_id', $group->id)->where('role', '=', 'sanawjy')->get();
      $time2 = TimeTable::where('group_id', $group->id)->where('role', '=', 'maydalawjy')->get();


      $dd = [];

      $count = 0;

      foreach ($time as $item) {

        $afternoon = json_decode($item['afternoon_time'], TRUE);
        $auditory = json_decode($item['auditor'], TRUE);
        $lessons = json_decode($item['lessons'], TRUE);
        $teachers = json_decode($item['teachers'], TRUE);
        $time = json_decode($item['time'], TRUE);
        $type_lessons = json_decode($item['type_lessons'], TRUE);


        $dd[$count]['my_lessons'] = explode(',', $lessons);
        $dd[$count]['my_teachers'] = explode(',', $teachers);
        $dd[$count]['my_time'] = explode(',', $time);
        $dd[$count]['my_time_afternoon'] = explode(',', $afternoon);
        $dd[$count]['auditory'] = explode(',', $auditory);
        $dd[$count]['type_lessons'] = explode(',', $type_lessons);
        $dd[$count]['day'] =  $item['day'];
        $dd[$count]['role'] =  $item['role'];

        $count++;
      }

      $d = [];
      $coun = 0;
      foreach ($time2 as $item) {

        $afternoon = json_decode($item['afternoon_time'], TRUE);
        $auditory = json_decode($item['auditor'], TRUE);
        $lessons = json_decode($item['lessons'], TRUE);
        $teachers = json_decode($item['teachers'], TRUE);
        $time = json_decode($item['time'], TRUE);
        $type_lessons = json_decode($item['type_lessons'], TRUE);


        $d[$coun]['my_lessons'] = explode(',', $lessons);
        $d[$coun]['my_teachers'] = explode(',', $teachers);
        $d[$coun]['my_time'] = explode(',', $time);
        $d[$coun]['my_time_afternoon'] = explode(',', $afternoon);
        $d[$coun]['auditory'] = explode(',', $auditory);
        $d[$coun]['type_lessons'] = explode(',', $type_lessons);
        $d[$coun]['day'] =  $item['day'];
        $d[$coun]['role'] =  $item['role'];

        $coun++;
      }

      return [
        'status' => true,
        'sanawjy' => $dd,
        'maydalawjy' => $d,
        'data' => $get_group

      ];
    }
  }


  public function getDataTeach(Request $request)
  {

    if ($request->id !== null) {
      $teacher = Teacher::with('getTeachTimeTable')->where('id', $request->id)->firstOrFail();
      $get_teacher = Teacher::where('id', $request->id)->first();
      $time = TeacherSchedule::where('teacher_id', $teacher->id)->where('role', '=', 'sanawjy')->get();
      $time2 = TeacherSchedule::where('teacher_id', $teacher->id)->where('role', '=', 'maydalawjy')->get();


      $dd = [];

      $count = 0;

      foreach ($time as $item) {

        $afternoon = json_decode($item['afternoon_time'], TRUE);
        $auditory = json_decode($item['auditor'], TRUE);
        $lessons = json_decode($item['lessons'], TRUE);
        $groups = json_decode($item['group'], TRUE);
        $time = json_decode($item['time'], TRUE);
        $type_lessons = json_decode($item['type_lessons'], TRUE);


        $dd[$count]['my_lessons'] = explode(',', $lessons);
        $dd[$count]['my_groups'] = explode(',', $groups);
        $dd[$count]['my_time'] = explode(',', $time);
        $dd[$count]['my_time_afternoon'] = explode(',', $afternoon);
        $dd[$count]['auditory'] = explode(',', $auditory);
        $dd[$count]['type_lessons'] = explode(',', $type_lessons);
        $dd[$count]['day'] =  $item['day'];
        $dd[$count]['role'] =  $item['role'];

        $count++;
      }

      $d = [];
      $coun = 0;
      foreach ($time2 as $item) {

        $afternoon = json_decode($item['afternoon_time'], TRUE);
        $auditory = json_decode($item['auditor'], TRUE);
        $lessons = json_decode($item['lessons'], TRUE);
        $groups = json_decode($item['group'], TRUE);
        $time = json_decode($item['time'], TRUE);
        $type_lessons = json_decode($item['type_lessons'], TRUE);


        $d[$coun]['my_lessons'] = explode(',', $lessons);
        $d[$coun]['my_groups'] = explode(',', $groups);
        $d[$coun]['my_time'] = explode(',', $time);
        $d[$coun]['my_time_afternoon'] = explode(',', $afternoon);
        $d[$coun]['auditory'] = explode(',', $auditory);
        $d[$coun]['type_lessons'] = explode(',', $type_lessons);
        $d[$coun]['day'] =  $item['day'];
        $d[$coun]['role'] =  $item['role'];

        $coun++;
      }

      return [
        'status' => true,
        'sanawjy' => $dd,
        'maydalawjy' => $d,
        'data' => $get_teacher

      ];
    }
  }

  public function search(Request $request)
  {

    $name = $request->name;
    $lastname = $request->last_name;
    $faculty = $request->faculty_name;
    $year = $request->year_number;
    $group = $request->group_number;



    if ($year != null && $faculty != null && $group != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('faculty_name', $faculty)
        ->where('group_number', $group)
        ->get();
      // $data = Group::with('getUsers')
      //             ->where('year_number', $year)
      //             ->where('faculty_name',$faculty)
      //             ->where('group_number',$group)
      //             ->get();

    } elseif ($year != null && $faculty != null && $group != null &&  $name != null && $lastname != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('faculty_name', $faculty)
        ->where('group_number', $group)
        ->where('last_name', $lastname)
        ->where('name', $name)
        ->get();

      // $data = Group::with('getUsers')
      //             ->where('year_number', $year)
      //             ->where('faculty_name',$faculty)
      //             ->where('group_number',$group)
      //             ->whereIn('get_users',$name)
      //             ->whereIn('get_users',$lastname)
      //             ->get();


    } elseif ($year != null && $faculty != null && $group != null &&  $lastname != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('faculty_name', $faculty)
        ->where('group_number', $group)
        ->where('last_name', $lastname)
        ->get();

      // $data = Group::with('getUsers')
      //             ->where('year_number', $year)
      //             ->where('faculty_name',$faculty)
      //             ->where('group_number',$group)
      //             ->whereIn('get_users',$lastname)
      //             ->get();


    } elseif ($year != null && $faculty != null && $group != null &&  $name != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('faculty_name', $faculty)
        ->where('group_number', $group)
        ->where('name', $name)
        ->get();

      // $data = Group::with('getUsers')
      //             ->where('year_number', $year)
      //             ->where('faculty_name',$faculty)
      //             ->where('group_number',$group)
      //             ->whereIn('get_users',$name)
      //             ->get();


    } elseif ($faculty != null && $group != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('group_number', $group)
        ->whereAnd('faculty_name', $faculty)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('group_number',$group)
      // ->whereAnd('faculty_name',$faculty)

    } elseif ($year != null && $faculty != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('faculty_name', $faculty)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('faculty_name',$faculty)
      // ->get();
    } elseif ($year != null  && $group != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('group_number', $group)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($year != null  && $name != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('name', $name)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($year != null  && $lastname != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->where('last_name', $lastname)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($group != null  && $lastname != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('group_number', $group)
        ->where('last_name', $lastname)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($group != null  && $name != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('group_number', $group)
        ->where('name', $name)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($faculty != null  && $lastname != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('faculty_name', $faculty)
        ->where('last_name', $lastname)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($faculty != null  && $name != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('faculty_name', $faculty)
        ->where('name', $name)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->where('group_number',$group)
      // ->get();
    } elseif ($year != null) {
      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('year_number', $year)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('year_number',$year)
      // ->get();
    } elseif ($group != null) {
      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('group_number', $group)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('group_number',$group)
      // ->get();
    } elseif ($faculty != null) {

      $data = DB::table('users')
        ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
        ->where('faculty_name', $faculty)
        ->get();

      // $data = Group::with('getUsers')
      // ->where('faculty_name',$faculty)
      // ->get();
    } else {

      if ($lastname != null  && $name != null) {

        $data = DB::table('users')
          ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
          ->where('name', $name)
          ->where('last_name', $lastname)
          ->get();

        // $data = User::with('getGroup')
        //             ->where('name', $name)
        //             ->where('last_name',$lastname)
        //             ->get();

      } elseif ($lastname != null) {
        $data = DB::table('users')
          ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
          ->where('last_name', $lastname)
          ->get();
      } elseif ($name != null) {
        $data = DB::table('users')
          ->leftJoin('groups', 'groups.id', '=', 'users.group_id')
          ->where('name', $name)
          ->get();
      }
    }

    // $key = \Request::get('q');

    //  $group = Group::with('getUsers')
    //     ->where('year_number',"{$key}")
    //     ->whereAnd('faculty_name','like',"{$key}")
    //     ->whereAnd('group_number','like',"{$key}")
    //     ->get();
    //  // ->orWhere('faculty_name', 'LIKE', "%{$key}%")
    //  // ->orWhere('year_number', 'LIKE', "%{$key}%")
    //  // ->orWhere('group_number', 'LIKE', "%{$key}%")

    //  $unit = User::with('getGroup')
    //    ->where('name',  "{$key}")
    //    ->orWhere('last_name','like',"{$key}")
    //    ->get();

    //dd($data == ['']);
    if ($data->isEmpty()) {
      return [
        'data' => "yalnyshlyk",
        'status' => false
      ];
    }
    return [
      'data' => $data,
      'status' => true
    ];
  }

  public function getUsers(Request $request, $id)
  {

    $group = Group::where('group_number', $id)->first();

    $users = User::where('group_id', $group->id)->get();

    return [
      'status' => true,
      'data' => $users
    ];
  }

  protected function respondWithUser($user,$data)
  {
    return response()->json([
      'status' => true,
      'access_token' => $user,
      'token_type' => 'bearer',
      'expires_in' => auth('users')->factory()->getTTL() * 60,
      'data' => $data
    ]);
  }

  protected function respondWithTeacher($teacher,$data)
  {
    return response()->json([
      'status' => true,
      'access_token' => $teacher,
      'token_type' => 'bearer',
      'expires_in' => auth('teachers')->factory()->getTTL() * 60,
      'data' => $data
    ]);
  }
}

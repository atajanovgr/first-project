<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{
    use HasFactory;
    protected $casts = [
        'time' => 'array',
        'lessons' => 'array',
        'teachers' => 'array'
    ];

    public  function getGroup(){

        return $this->hasOne('App\Models\Group','id','group_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    use HasFactory;
    public  function getFacultyGroup(){

        return $this->hasMany('App\Models\Group','faculty_id','id');
    }
}

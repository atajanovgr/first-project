<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    public  function getUsers(){

        return $this->hasMany('App\Models\User','group_id','id',);
    }

    public  function getTimeTable(){

        return $this->hasMany('App\Models\TimeTable','group_id','id');
    }

   
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\GroupsController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\TimeTableController;
use App\Http\Controllers\AuthTeachControler;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/get-groups', [AuthController::class, 'getGroups']);
    Route::get('/get-time-table', [AuthController::class, 'getData']);
    Route::get('/get-faculty', [AuthController::class, 'getFaculty']);
    Route::get('/get-users/{id}', [AuthController::class, 'getUsers']);

});

Route::group([
    'prefix' => 'teach'

], function ($router) {

    Route::get('/get-groups', [AuthTeachControler::class, 'getGroups']);
    Route::get('/get-teach-time-table', [AuthTeachControler::class, 'getDataTeach']);
    Route::get('/get-time-table', [AuthTeachControler::class, 'getData']);
    Route::post('/search', [AuthTeachControler::class, 'search']);
    Route::get('/get-faculty', [AuthTeachControler::class, 'getFaculty']);
    Route::get('/get-users/{id}', [AuthTeachControler::class, 'getUsers']);

});

// Route::group(['prefix' => 'teach','middleware' => ['assign.guard:teachers','jwt.auth']],function ()
// {
//     Route::post('/login', [AuthController::class, 'login']);
//     Route::get('/get-groups', [AuthController::class, 'getGroups']);
//     Route::get('/get-time-table', [AuthController::class, 'getData']);
//     Route::get('/get-faculty', [AuthController::class, 'getFaculty']);
// });

// Route::group(['prefix' => 'auth','middleware' => ['assign.guard:users','jwt.auth']],function ()
// {
//     Route::post('/login', [AuthController::class, 'login']);
//     Route::get('/get-groups', [AuthController::class, 'getGroups']);
//     Route::get('/get-time-table', [AuthController::class, 'getData']);
//     Route::get('/get-teach-time-table', [AuthController::class, 'getDataTeach']);
//     Route::post('/search', [AuthController::class, 'search']);
//     Route::get('/get-faculty', [AuthController::class, 'getFaculty']);
//     Route::get('/get-users/{id}', [AuthController::class, 'getUsers']);
// });
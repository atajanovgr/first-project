<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\GroupsController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\TimeTableController;
use App\Http\Controllers\Api\FacultyController;
use App\Http\Controllers\Api\TeachersController;
use App\Http\Controllers\Api\ScheduleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {

    Route::resource('groups', GroupsController::class);
    Route::resource('faculty', FacultyController::class);
    Route::get('/faculty/delete/{id}', [FacultyController::class, 'delete'])->name('faculty.delete');
    Route::get('/groupss/delete/{id}', [GroupsController::class, 'delete'])->name('groups.delete');

    Route::resource('users', UsersController::class);
    Route::get('/userss/delete/{id}', [UsersController::class, 'delete'])->name('users.delete');

    Route::resource('teachers', TeachersController::class);
    Route::get('/teachers/delete/{id}', [TeachersController::class, 'delete'])->name('teachers.delete');

    Route::resource('time_table', TimeTableController::class);
    Route::get('/time_tables/delete/{id}', [TimeTableController::class, 'delete'])->name('time_tables.delete');

    Route::resource('teacher_time_table', ScheduleController::class);
    Route::get('/teacher_time_tables/delete/{id}', [ScheduleController::class, 'delete'])->name('teacher_time_tables.delete');

});
